// http://21st-century-spring.kr/wordpress/?page_id=794

#include <SoftwareSerial.h>
#include <Wire.h>
#include "RTClib.h"
#include <Adafruit_Fingerprint.h>

RTC_DS1307 rtc;
SoftwareSerial mySerial(2, 3);

enum LOCK_STATUS {CLOSE, OPEN};

Adafruit_Fingerprint finger = Adafruit_Fingerprint(&mySerial);

int lockStatus = 0;
int hourTime;
int minuteTime;
int secondsTime;
int dayNumber;
int fingerprintID = 0;


int buzzerPin = 4;


void setup() {
  // put your setup code here, to run once:
  pinMode(13, OUTPUT);
  pinMode(buzzerPin, OUTPUT);
  digitalWrite(13, LOW);
  Serial.begin(9600);

  finger.begin(57600);

   if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }

  if (! rtc.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }
  //rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
}

void loop() {
  // put your main code here, to run repeatedly:

  DateTime now = rtc.now();

  hourTime = now.hour();
  minuteTime = now.minute();
  secondsTime = now.second();
  dayNumber = now.dayOfTheWeek();

  fingerprintID = getFingerprintID();
  
  if ((fingerprintID == 1 || fingerprintID == 2 || fingerprintID == 3 || fingerprintID == 4)) {
    if (lockStatus == CLOSE) {
      digitalWrite(13, HIGH);
      lockStatus = OPEN;
    } else {
      digitalWrite(13, LOW);
      lockStatus = CLOSE;
    }
  }

  if (hourTime == 17 && minuteTime == 06 && secondsTime == 0 && (dayNumber != 6) && dayNumber != 7) {
    digitalWrite(13, HIGH);
    lockStatus = OPEN;
  }

  if ((hourTime == 17) && (minuteTime == 07) && (secondsTime == 0) && dayNumber != 6 && dayNumber != 7) {
    digitalWrite(buzzerPin, HIGH);
    delay(5000);
    digitalWrite(buzzerPin, LOW);
  }
  
  if ((hourTime == 17) && (minuteTime == 07) && (secondsTime == 10) && (dayNumber != 6 && dayNumber != 7)) {
      digitalWrite(13, LOW);
      lockStatus = CLOSE;
  }

  Serial.print("HOUR: ");
  Serial.print(hourTime);
  Serial.print(" ");
  Serial.print("MINUTE: ");
  Serial.print(minuteTime);
  Serial.print(" ");
  Serial.print("DAY: ");
  Serial.println(dayNumber);
  
}



int getFingerprintID() {
  uint8_t p = finger.getImage();
  if (p != FINGERPRINT_OK)  return -1;

  p = finger.image2Tz();
  if (p != FINGERPRINT_OK)  return -1;

  p = finger.fingerFastSearch();
  if (p != FINGERPRINT_OK)  return -1;
  
  // found a match!
  //Serial.println("Found ID #"); Serial.print(finger.fingerID); 
  //Serial.print(" with confidence of "); Serial.println(finger.confidence);
  return finger.fingerID; 
}